import Prelude hiding (Semigroup((<>)), Monoid)

data List a = Nil | Cons a (List a)
  deriving (Eq, Show)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons h t) = Cons (f h) (fmap f t)

data Optional a = Empty | Full a
  deriving (Eq, Show)

class Semigroup a where
  (<>) :: a -> a -> a

class Semigroup a => Monoid a where
  identity :: a

instance Semigroup Int where
  -- addition
  (<>) = error "todo: instance Semigroup Int"

instance Monoid Int where
  identity = 0

instance Semigroup (List a) where
  (<>) = error "todo: instance Semigroup (List a)"

instance Monoid (List a) where
  identity = error "todo: instance Monoid (List a)"

instance Semigroup (Optional a) where
  (<>) = error "todo: instance Semigroup (Optional a)"

instance Monoid (Optional a) where
  identity = error "todo: instance Monoid (Optional a)"

-- need to wrap a function due to Haskell reasons
data Endomorphism a = Endomorphism (a -> a)

runEndomorphism :: Endomorphism a -> a -> a
runEndomorphism (Endomorphism f) = f

instance Semigroup (Endomorphism a) where
  (<>) = error "todo: instance Semigroup (Endomorphism a)"

instance Monoid (Endomorphism a) where
  identity = error "todo: instance Monoid (Endomorphism a)"

data Predicate a = Predicate (a -> Bool)

-- to join two predicates
-- both bool values must be True (&&)
instance Semigroup (Predicate a) where
  (<>) = error "todo: instance Semigroup (Predicate a)"

instance Monoid (Predicate a) where
  identity = error "todo: instance Monoid (Predicate a)"

data Minimum = Minimum Int
  deriving (Eq, Show)

-- find the minimum of two Int values
instance Semigroup Minimum where
  (<>) = error "todo: instance Semigroup Minimum"

instance Monoid Minimum where
  identity = error "todo: instance Monoid Minimum"

-- if a and b are semigroups, then (a, b) is a semigroup
instance (Semigroup a, Semigroup b) => Semigroup (a, b) where
  (<>) = error "todo: instance Semigroup (a, b)"

-- if a and b are monoids, then (a, b) is a monoid
instance (Monoid a, Monoid b) => Monoid (a, b) where
  identity = error "todo: instance Monoid (a, b)"

-- foldRight does constructor replacement
-- foldRight f z will replace in the given list
-- * Cons with f
-- * Nil with z
foldRight :: (a -> b -> b) -> b -> List a -> b
foldRight _ b Nil      = b
foldRight f b (Cons h t) = f h (foldRight f b t)

-- add up the numbers in a list
-- use foldRight
-- utilise instance Monoid Int
addUp :: List Int -> Int
addUp = error "todo: addUp"

-- list of functions
listOfFunctions :: List (Endomorphism Int)
listOfFunctions = Endomorphism <$>
  Cons (+1)
  ((Cons (*2))
  ((Cons (div 3))
  ((Cons (+99))
  (Cons (subtract 7) Nil))))

-- a function that applies the list of functions (listOfFunctions)
-- to the given integer
-- see: runEndomorphism
buildUp :: Int -> Int
buildUp = error "todo: buildUp"

-- add up the integers in the list
-- join all the predicates (&&)
addAnd :: List (Int, Predicate String) -> (Int, Predicate String)
addAnd = error "todo: buildUp"

